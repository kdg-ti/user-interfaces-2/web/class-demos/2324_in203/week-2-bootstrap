import {Modal} from "bootstrap";

const myModalEl = document.getElementById('exampleModal') as HTMLElement;
const myModal : Modal = new Modal(myModalEl);

myModalEl.addEventListener('hidden.bs.modal', event => {
    console.log("closed modal, will always run when modal is closed")
})

document.getElementById("modal-save")!.addEventListener('click', event => {
    console.log("save changes!");
    myModal.hide();
})